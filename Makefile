VERSION=0.3
NAME=spkitlasso
DIR:=$(shell basename `pwd`)
FULL=$(NAME)-$(VERSION)
ZIP=$(FULL).zip
TGZ=$(FULL).tar.gz
DATAPREFIX=/usr/share
DATADIR=$(DATAPREFIX)/$(NAME)
PHPINCLUDEDIR=/usr/share/php
DOCPREFIX=/usr/share/doc
DOCDIR=$(DOCPREFIX)/$(NAME)
HTTPD_USER=www-data
VARPREFIX=/var/lib
VARDIR=$(VARPREFIX)/$(NAME)

.PHONY: zip distclean dist install all clean install-directories install-files

all:


zip:
	make install-standalone DESTDIR=.
	cd build; zip -r ../$(ZIP) *
	rm -rf build

dist: distclean
	mkdir $(FULL)
	cp -R debian doc exemples endpoints include README Makefile INSTALL TODO $(FULL)
	rm -f ../$(TGZ)
	tar cvzf ../$(TGZ) $(FULL)


install-directories:
	install -d $(DESTDIR)$(DATADIR) $(DESTDIR)$(DATADIR)/include $(DESTDIR)$(DATADIR)/endpoints $(DESTDIR)$(PHPINCLUDEDIR) $(DESTDIR)$(DOCDIR)/exemples
	install -d $(DESTDIR)$(DOCDIR)/html
	install -d $(DESTDIR)$(DOCDIR)
	install -d -o $(HTTPD_USER) -m 755 $(DESTDIR)$(VARDIR)
	ln -nfs $(DATADIR)/include $(DESTDIR)$(PHPINCLUDEDIR)/$(NAME)

install-files:
	install -m 644 -t $(DESTDIR)$(DATADIR)/include include/*
	install -m 644 -t $(DESTDIR)$(DATADIR)/endpoints endpoints/*
	install -m 644 -t $(DESTDIR)$(DOCDIR)/html doc/*.html
	install -m 644 -t $(DESTDIR)$(DOCDIR) README INSTALL
	install -m 644 -t $(DESTDIR)$(DOCDIR)/exemples exemples/*.*

install-standalone:
	mkdir -p $(DESTDIR)/build/spkitlasso
	mkdir -p $(DESTDIR)/build/data
	cp endpoints/* $(DESTDIR)/build
	cp exemples/*  $(DESTDIR)/build/
	cp include/*.php $(DESTDIR)/build/spkitlasso/
	sed -i 's#^\( *\)\$$path#\1// $$path#;n;s#// \$$path#$$path#' $(DESTDIR)/build/spkitlasso/*datadir*php

uninstall:
	rm -rf $(DESTDIR)$(DATADIR) $(DESTDIR)$(DOCDIR) $(DESTDIR)$(PHPINCLUDEDIR)/$(NAME) $(DESTDIR)$(VARDIR)

install: install-directories install-files

distclean:
	rm -rf $(FULL)

clean:
	-rm -rf $(ZIP)
