<?php
require_once('lassospkit_utils_session.inc.php');
require_once('lassospkit_utils.inc.php');
require_once('lassospkit_config.inc.php');
require_once("lassospkit_helper.inc.php");
require_once("lassospkit_saml2.inc.php");
require_once("lassospkit_metadata.inc.php");
require_once("lassospkit_utils.inc.php");
require_once("lassospkit_utils_session.inc.php");
require_once("lassospkit_dummysession.inc.php");
require_once("lassospkit_autopersistentsession.inc.php");


/** This file contains the public front-end API
 to the LassoSPKit for PHP. 

 The idea is to limit interaction at the minimum.
 If lassospkit_nameid() is null the you are not logged
 to an IdP.
 To obtain the nameID associated with login to an IdP,
 just redirect the user's browser using 
 lassospkit_websso_redirect($allowCreate). The allowCreate
 defines if you want a new federation created or only accept
 previously defined federation (the case if you wan to retrieve
 a previously established federation).
 */

/** Build an url for redirecting to one of the Liberty endpoints,
 use $endpoint as the endpoint name, $return_url as the destrination url
 after Liberty transaction, and params as key-value dictionarry for generating other params. */


function _lassospkit_make_redirect_url($endpoint, $return_url, $params) {
    // Endpoints base
    $redirect = LassoSPKitConfig::get('baseUrl');
    // saml2 or liberty
    $redirect = $redirect . '/' . LassoSPKitConfig::get('conformance');
    if (LassoSPKitConfig::get('showExtension')) {
        $redirect = $redirect . '.php';
    }
    // Specific endpoint
    $redirect .= '/' . $endpoint;
    // Return url param
    $redirect = $redirect . "?return_url=" . urlencode($return_url);
    // Other params
    foreach ($params as $key => $value) {
        if (is_bool($value)) {
            $value = intval($value);
        }
        $redirect = $redirect . '&' . urlencode($key) . "=" . urlencode($value);
    }
    return $redirect;
}

/** If this session contains the result of a recent WebSSO return
 the retrieved nameID. */
function lassospkit_nameid() {
    return LassoSPKitUtilsSession::getNameID();
}

function lassospkit_set_nameid($nameid) {
    LassoSPKitUtilsSession::setNameID($nameid);
}

/** Returns the userid associated to the current, if any. */
function lassospkit_userid() {
    return LassoSPKitUtilsSession::getUserID();
}

/** If last interaction resulted in an error, 
 * return a human readable description. */
function lassospkit_error() {
    return LassoSPKitUtilsSession::getLastError();
}

/** Return an opaque string representing 
 federation informations as result of the last
 liberty exchange.
 */
function lassospkit_federation() {
    return LassoSPKitUtilsSession::getFederation();
}

/** Set the opaque string representing 
 federation informations as result of the last
 liberty exchange.
 */
function lassospkit_set_federation($federation) {
    LassoSPKitUtilsSession::setFederation($federation);
}

/* Return the URL where to redirect a user to ask for 
 * an authentification assertion from the IdP without creating
 * new federation.
 * Second arguments is an array of options. Possible options are:
 * - "persistent" => Bool, wether to get an existing persistent
 *    session or get a one-time federation identifier.
 * - "passive" => Bool, forbid user interaction by the IdP.
 * For example, to verify "transparently" (there is still the redirect latency)
 * if an user is currently logged to the IdP, just do:
 * header("Location: " . lassospkit_login_url($login_page, array("passive" => TRUE)))
 *
 * DEPRECATED
 *
 */
function lassospkit_login_url($return_url, $options = array() ) {
    $params = LassoSPKitUtils::extract_options(array('persistent' => 'b', 'passive' => 'b'), $options);
    return _lassospkit_make_redirect_url('login',$return_url, $params);
}

/* Return the URL where to redirect a user to create a new federation, or
 * get an existing one.
 * Eventually pass a username to auto-store the new federation. DEPRECATED
 */
function lassospkit_federate_url($return_url) {
    return _lassospkit_make_redirect_url('federate',$return_url, array());
}

/** Sets the userid to associate to this nameID, during
 the next WebSSO interaction.
 Only useful if you activated autofederation. */
function lassospkit_set_userid($userid) {
    LassoSPKitUtilsSession::setUserID($userid);
}

/* Return the URL where to redirect a user to initiated defederation of the current nameid.  DEPRECATED*/
function lassospkit_defederation_url($return_url) {
    return _lassospkit_make_redirect_url('defederate',$return_url, array());
}
/** DEPRECATED */
function lassospkit_logout_url($return_url) {
    return _lassospkit_make_redirect_url('slo',$return_url, array());
}

/** DEPRECATED */
function lassospkit_configure_url() {
    $url = LassoSPKitConfig::get('baseUrl') . '/configure';
    // saml2 or liberty
    if (LassoSPKitConfig::get('showExtension')) {
        $url = $url . '.php';
    }
    return $url;
}

function lassospkit_redirect_login($relayState = null,$isPassive = false) {
    $session_class = "LassoSPKit" . LassoSPKitConfig::get('session');
    $session = new $session_class();
    $saml2 = new LassoSPKitSAML2($session);
    $saml2->ssoInit(array('relayState' => $relayState, 'allowCreate'=>FALSE, 'isPassive' => $isPassive));
}

function lassospkit_redirect_federate($relayState = null,$isPassive = false) {
    $session_class = "LassoSPKit" . LassoSPKitConfig::get('session');
    $session = new $session_class();
    $saml2 = new LassoSPKitSAML2($session);
    $saml2->ssoInit(array('relayState' => $relayState, 'allowCreate'=>TRUE, 'isPassive' => $isPassive));
}

function lassospkit_get_assertion_attributes() {
    $session = LassoSPKitUtilsSession::getSingleton();
    $attributes = $session->getAssertionAttributes();
    return $attributes;
}
/** Generate a soap logout message for the session associated
 to the current nameId's. If none is present try to find one using
 the userId associated to the nameId. If it is still impossible
 to find one session return an error code.
 */
function lassospkit_soap_logout() {
    try {
        $session_class = "LassoSPKit" . LassoSPKitConfig::get('session');
        $session = new $session_class();
        $saml2 = new LassoSPKitSAML2($session);
        $ret = $saml2->initiateSLO(LASSO_HTTP_METHOD_SOAP);
    } catch (LassoError $e) {
        $ret = $e->getCode();
    }
    return $ret;
}

/** Generate and send a SOAP logout request for the defederation 
 of the first nameId found. If none is present try to find one using
 the userId associated to the nameId. If it is still impossible
 to find ones identity return an error code. */
function lassospkit_soap_defederate() {
    try {
        $session_class = "LassoSPKit" . LassoSPKitConfig::get('session');
        $session = new $session_class();
        $saml2 = new LassoSPKitSAML2($session);
        $ret = $saml2->initiateFTNotification(LASSO_HTTP_METHOD_SOAP);
    } catch (LassoError $e) {
        $ret = $e->getCode();
    }
    lassospkit_clean();
    return $ret;
}

/** Reset the sate of the spkit, clear cache of userid, nameid and attributes
 retrieved by precedent liberty exchanges. */
function lassospkit_clean() {
    $session = LassoSPKitUtilsSession::getSingleton();
    $session->clean();
}
