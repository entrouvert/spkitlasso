<?php
require_once('lassospkit_datadir.inc.php');
require_once('lassospkit_config.inc.php');
require_once('lassospkit_debug.inc.php');
require_once('lassospkit_saml_common.inc.php');
require_once('lassospkit_lib.inc.php');

class LassoSPKitSaml2 extends LassoSPKitSAMLCommon {
    public function __construct(LassoSPKitGenericSession $session) {
        parent::__construct($session);
	if (!$this->initServer(lassospkit_datadir() . "/saml2")) {
		throw new Exception("Construction de l'objet SAML2 impossible");
        }
    }
    /** Overloaded method to serve as callback to common SSO method. */
    public function ssoNameIdPolicyConfig(LassoLogin $login, $blob) {
            $request = $login->request;
            $nameidpolicy = $request->NameIDPolicy;
            $nameidpolicy->format = $blob['nameIDFormat'];
            $nameidpolicy->allowCreate = $blob['allowCreate'];
    }
    /** Initiate a SSO exchange with a redirect.
        The federate parameter defines if we wish to get a
        persistent (federate = TRUE) or a transient federation
        (federate = FALSE).
        The create parameter specify if the IdP is allower
        to create a new federation if a persistent
        one is asked for federate = TRUE.
      */
    function sso($create = TRUE, $federate = TRUE, $passive = FALSE, $relayState = null) {
        if ($federate) {
            $format = LASSO_SAML2_NAME_IDENTIFIER_FORMAT_PERSISTENT;
        } else {
            $format = LASSO_SAML2_NAME_IDENTIFIER_FORMAT_TRANSIENT;
        }
        return $this->ssoInit(array('allowCreate' => $create,  'nameIDFormat' => $format, 'isPassive' => $passive, 'relayState' => $relayState));
    }
    function ssoInit($params = array())
    {
        $default_params = array( 
            'allowCreate' => TRUE,
            'nameIDFormat' => LASSO_SAML2_NAME_IDENTIFIER_FORMAT_PERSISTENT,
            'remoteID' => null,
            'method' => LASSO_HTTP_METHOD_REDIRECT,
            'isConsentObtained' => FALSE,
            'forceAuthn' => FALSE,
            'relayState' => null,
            'isPassive' => FALSE);
        $params = array_merge($default_params, $params);
        extract($params);
        lassospkit_debuglog("Params isPassive: $isPassive allowCreate: $allowCreate format: $nameIDFormat", 1);
        $login = null;
        return parent::ssoCommon($login, $remoteID, $method, $isConsentObtained, $forceAuthn, $isPassive, $relayState, array('nameIDFormat'=>$nameIDFormat, 'allowCreate' => $allowCreate));
    }
    /** Defederation, convert to NameIdManagement protocol **/
    public function initiateFTNotification($method = LASSO_HTTP_METHOD_SOAP,$remoteID = null)
    {
        $ok = $this->initiateNameIdManagement(null, $method, $remoteID);
        parent::initiateFTNotification($method,$remoteID);    
        return $ok;
    }
    /** Name Id Management, SP inititated */
    /* Request */
    public function initiateNameIdManagement($newNameID, $method = LASSO_HTTP_METHOD_SOAP, $remoteID = null) {
        $code = null;
        $response = null;
        $nidmanagement = new LassoNameIdManagement($this->server);
        $this->findFederation($nidmanagement);
        $retIR = $nidmanagement->initRequest($remoteID, $newNameID, $method);
        $retBRM = $nidmanagement->buildRequestMsg();
        if ($retIR || $retBRM) {
            lassospkit_errlog("initiateNIDManagement: retIR: $retIR retBRM: $retBRM");
        }
        switch ($method) {
            case LASSO_HTTP_METHOD_REDIRECT:
                $this->keepProfile($nidmanagement);
                $this->finishRedirectRequest($nidmanagement);
                break;
            case LASSO_HTTP_METHOD_SOAP:
                $this->finishSOAPRequest($nidmanagement, $response, $code);
                $retPRNIM = $this->processResponseNameIdManagement($response, $nidmanagement);
                break;
            case LASSO_HTTP_METHOD_ARTIFACT_GET:
            case LASSO_HTTP_METHOD_ARTIFACT_POST:
            case LASSO_HTTP_METHOD_POST:
            default:
                LassoSPKitHelper::notImplemented();
        }
        $this->checkXmlErrors("NameIdManagement");
    }
    function processRedirectResponseNameIdManagement() {
        $nidmanagement = null;
        $this->processResponseNameIdManagement($_SERVER['QUERY_STRING'], $nidmanagement);
    }
    /** Response **/
    public function processResponseNameIdManagement($message, &$nidmanagement)
    {
        if ($nidmanagement == null) {
            $nidmanagement = LassoNameIdManagement::newFromDump($this->server, $this->restoreProfile());
            $this->findFederation($nidmanagement);
        }
        $retPRM = $nidmanagement->processResponseMsg($message);
        $this->changeFederation($nidmanagement);
    }
    /** Name Id Management request IdP iniated */
    public function processRedirectRequestNameIdManagement() {
        return processRequestNameIdManagement(LASSO_HTTP_METHOD_REDIRECT,
                                 $_SERVER['QUERY_STRING']);
    }
    public function processSOAPRequestNameIdManagement() {
        $contents = $this->receiveSoapMessage();
        return $this->processRequestNameIdManagement(LASSO_HTTP_METHOD_SOAP,
                                 $contents);
    }
    public function processRequestNameIdManagement($method, $message)
    {
        $nidmanagement = new LassoNameIdManagement($this->server);
        $retPRM = $nidmanagement->processRequestMsg($message);
        $this->findFederation($nidmanagement);
        $retVR = $nidmanagement->validateRequest();
        if ($retPRM || $retVR) {
            lassospkit_errlog("initiateNIDManagement: retPRM: $retPRM retVR: $retVR");
        }
        $retCF = $this->changeFederation($nidmanagement);
        $retFR = $this->finishResponse($nidmanagement, $method);
        if ($retPRM) {
            return $retPRM;
        }
        if ($retVR) {
            return $retVR;
        }
        if ($retCF) {
            return $retCF;
        }
        if ($retFR) {
            return $retFR;
        }
        return 0;
    }
    /** Change federation with respect to nidmanagement request content.
        If a response is present
    */
    function changeFederation(LassoNameIdManagement $nidmanagement) {
        LassoSPKitHelper::changeFederation($nidmanagement, 
                    $this->session, 
                    $nidmanagement->request->NewID); 
        if ($nidmanagement->nameIdentifier) {
            $this->nameID = LassoSPKitHelper::profileGetNameID($nidmanagement);
        }
    }
}
