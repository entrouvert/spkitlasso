<?php
require_once('lassospkit_generic_session.inc.php');
require_once('lassospkit_utils_session.inc.php');
require_once('lassospkit_debug.inc.php');

class LassoSPKitDummySession extends LassoSPKitGenericSession {
    /** Save the federation into the SESSION object */
    function saveFederation() {
        LassoSPKitUtilsSession::setFederation(
            serialize($this->getFederationArray()));
        parent::saveFederation();
    }
    /** Use the nameID as a hint to validate the stored dumps.
     It not present use them directly. */
    function findFederation($nameID) {
        $federation = LassoSPKitUtilsSession::getFederation();
        $ret = explodeFederationBlob($federation);
        if ($ret == 1 && $nameID) {
            $nameIDs = $this->getNameIDs();
            if (! @in_array($nameID, $nameIDs)) {
                $this->setSessionDump(null);
                $this->setIdentityDump(null);
                LassoSPKitUtilsSession::setUserId(null);
                return 0;
            }
        }
        return $ret;
    }
}
