<?php

/** This class implements dummy methods for
 generic handling of SSO session i.e storage
 of federation data (identity_dump and 
 session_dump), retrieval of attributes,
 etc.. */
class LassoSPKitGenericSession {
    public $exception;
    private $session_dump;
    private $identity_dump;
    private $attributes;

    function processAttributes(array $attributes) {
    }
    function doRedirect($url) {
        header("Location: $url");
    }
    function doResponse($mimeType, $content) {
        lassospkit_debuglog("Renvoi la réponse de type $mimeType et contenu $content", 1);
        header("Content-type: $mimeType");
        echo $content;
    }
    function logout() {
    }
    /** Save session_dump and identity_dump,
      use any mean to link them to a local session. 
      The default implementation just report the list of nameIDs
      in the nameIDs session variable.
      */
    function saveFederation() {
        $identity_dump = $this->getIdentityDump();
        $nameIDs = LassoSPKitHelper::getNameIDsFromDump($identity_dump);
        LassoSPKitUtilsSession::setNameID($nameIDs);
    }
    function findFederation($nameID) {
    }
    function changeFederation($nameID, $NewID) {
    }
    function setSessionDump($dump) {
        $this->session_dump = $dump;
    }
    function setIdentityDump($dump) {
        $this->identity_dump = $dump;
    }
    function getSessionDump() {
        return $this->session_dump;
    }
    function getIdentityDump() {
        return $this->identity_dump;
    }
    function getNameIDs() {
        $identity_dump = $this->getIdentityDump();
        return LassoSPKitHelper::getNameIDsFromDump($identity_dump);
    }
    function getFederationArray() {
            $userid = LassoSPKitUtilsSession::getUserID();
            return array(
                'identity'=> $this->getIdentityDump(), 
                'session' => $this->getSessionDump(), 
                'userid'=>$userid,
            );
    }
    function explodeFederationBlob($blob) {
        $federation = @unserialize($blob);
        if ($federation === FALSE) {
            lassospkit_debuglog("LassoSPKitGenericSession: cannot deserialize the federation blob", 1);
            return 0;
        }
        $this->explodeFederation($federation);
    }
    function explodeFederation($federation) {
        $this->setSessionDump($federation['session']);
        $this->setIdentityDump($federation['identity']);
        if ($federation['userid'] && ! LassoSPKitUtilsSession::getUserID()) {
            LassoSPKitUtilsSession::setUserID($federation['userid']);
        }
        return 1;
    }
}
