<?php
require_once('lassospkit_datadir.inc.php');
require_once('lassospkit_helper.inc.php');

/** This class generate the WebUI for configuring the SP kit. */
class LassoSPKitConfigUIGen 
{
    private $special_input;
    private $default_input;
    private $base;

    function __construct() {
        $suffix = "";
        if (LassoSPKitConfig::get('showExtension')) {
            $suffix = ".php";
        }
        $this->special_input = array(
                'organization' =>
                array('Nom du service',
                    'text'),
                'baseUrl' =>
                array('Base des URLS des points d\'accès', 'text'),
                'gruik' =>
                array('URL des métadatas SAML 2.0 du SP',
                    'url',
                    LassoSPKitUtils::relativePathToURL("saml2" . $suffix . "/metadata")),
                'gruik2' =>
                array('URL des métadatas Liberty 1.2 du SP',
                    'url',
                    LassoSPKitUtils::relativePathToURL("liberty" . $suffix . "/metadata")),
                'idp_metadata_url' => 
                array('URL des métadatas du service d\'authentification',
                    'text'),
                'idp_metadata' => 
                array('Métadatas de l\'IdP',
                    'textarea'),
                'clear_pkey' =>
                array('Génère une clé privée, même si une existe déjà',
                    'checkbox'),
                'federate' => 
                array('Stockage de la fédération',
                    'select',
                    array('no' => 'Non',
                        'MySql' => 'Via MySql',
                        'File' => 'Via le filesystem')),
                'mysql_host' => array('Host de la base MySql'),
                'mysql_user' => array('Identifiant sur la base'),
                'mysql_password' => array('Mot de passe sur la base', 'password'),
                'mysql_database' => array('Nom de la base'),
                'mysql_table' => array('Nom de la table'),
                'lasso_lib' => array('Emplacement de la bibliothèque Lasso PHP'),
                'cookiename' => array('Cookiename', 'text'),
                'default_return_url' => array('URL de retour par défaut', 'text'));
    }
    function itype($name) {
        if (isset($this->special_input[$name][1])) {
            return $this->special_input[$name][1];
        }
        return 'text';
    }
    function icaption($name) {
        if (isset($this->special_input[$name][0])) {
            return $this->special_input[$name][0];
        }
        return $name;
    }
    function value($name) {
        switch ($name) {
            case 'idp_metadata':
                return @file_get_contents(LassoSPKitHelper::getIdpMetadataFile());
            case 'baseUrl':
                return LassoSPKitUtils::mydir();
        }
        $val = null;
        try {
            $val = LassoSPKitConfig::get($name);
        } catch (Exception $e) {
        }
        if ($val) {
            return $val;
        } else {
            return '';
        }
    }

    function msg($mess) {
        echo htmlspecialchars(gettext($mess));
    }
    function render_form() {
        echo '<head>', "\n";
        echo '<title>';
        $this->msg('Configuration du SP-Kit LassoSPKit');
        echo '</title>', "\n";
        echo '<body>', "\n";
        echo '<h1>', "\n";
        $this->msg('Configuration du SP-kit LassoSPKit');
        echo '</h1>', "\n";
        echo '<form action="' . htmlspecialchars(LassoSPKitUtils::myself()) . '" method="post">', "\n";
        echo '   <table>', "\n";
        foreach ($this->special_input as $k => $v) {
            echo '<tr><td>';
            $this->msg($this->icaption($k));
            echo '</td><td>';
            $actual = $this->value($k);
            $type = $this->itype($k);
            switch ($type) {
                case 'textarea':
                    echo '<textarea name="' . $k . '" cols="80" row="20">';
                    $this->msg($actual);
                    echo '</textarea>';
                    break;
                case 'select':
                    echo '<select name="' . $k . '" size="1">';
                    foreach ($v[2] as $x => $y) {

                        echo '<option value="' . $x . '"';
                        if ($actual == $x) {
                            echo ' selected ';
                        }
                        echo '>';
                        $this->msg($y);
                        echo '</option>';
                    }
                    echo '</select>';
                    break;
                case 'text':
                    echo '<input name="' . $k . '"';
                    echo 'type="text" size="80" value="' . $actual . '"/>';
                    break;
                case 'url':
                    echo '<a href="';
                    $this->msg($v[2]);
                    echo '">';
                    $this->msg($v[2]);
                    echo '</a>';
                    break;
                default:
                    echo '<input name="' . $k . '"';
                    echo 'type="' . $type . '" ';
                    if ($actual) {
                        echo 'value="' . $actual . '" ';
                    }
                    echo '/>';
                    break;
            }
            echo '</td></tr>', "\n";
        }
        echo '<tr><td/><td><input type="submit" value="Modifier"/></td></tr>', "\n";
        echo '</table>', "\n";
        echo '</body>', "\n";
    }
    function importConfigFromPost($post) {
        foreach (LassoSPKitConfig::keys() as $k) {
            if (isset($post[$k]))
            {
                LassoSPKitConfig::set($k, $post[$k]);
            }
        }
        LassoSPKitConfig::commit();
    }

    function validateMetadata($file) {
        return LassoSPKitHelper::getConformance($file);
    }
    function getIdpMeta($idpMeta, $idpMetaUrl, &$conformance, &$error) {
        if (! $idpMeta && ! $idpMetaUrl) {
            throw new Exception("illegal argument");
        }
        $file = tempnam(TEMPDIR, "idp-metadata-temp");
        $ok = 0;
        if ($idpMetaUrl) {
            $idpMeta = @file_get_contents($idpMetaUrl);
            $ok = ($idpMeta != null);
            if ($ok) {
                $ok = @file_put_contents($file, $idpMeta);
                if ($ok) {
                    $conformance = $this->validateMetadata($file);
                    $ok = ($conformance != -1);
                if (! $ok) {
                    $error = "GetIDPMeta: Retrieved metadatas non conformant";
                }
                } else {
                    $error = "GetIDPMeta: Cannot store retrieved IdP metadatas";
                }
            } else {
                $error = "GetIDPMeta: Cannot retrieve metadatas from the given URL: $idpMetaUrl";
            }
        }
        if (! $ok && $idpMeta) {
            $ok = @file_put_contents($file, $idpMeta);
            if ($ok) {
                $conformance = $this->validateMetadata($file);
                $ok = ($conformance != -1);
                if (! $ok) {
                    $error = "GetIDPMeta: Metadatas non conformant";
                }
            } else {
                $error = "GetIDPMeta: Cannot store given IdP metadatas";
            }
        }
        if ($ok) {
            return $file;
        } else {
            @unlink($file);
            return FALSE;
        }

    }
    function generateSPMetadata($conformance, $organization, $publickey, &$error) {
        $spmeta = LassoSPKitHelper::getMetadataDir($conformance) . "/" . SP_METADATA;
	$base = LassoSPKitConfig::get('baseUrl');
        switch ($conformance) {
            case LASSO_PROTOCOL_SAML_2_0:
                $meta = LassoSPKitMetadataSAML2::generateMetadata($base, $organization, array('publickey' => $publickey));
                LassoSPKitConfig::set('conformance', 'saml2');
                break;
            case LASSO_PROTOCOL_LIBERTY_1_0:
            case LASSO_PROTOCOL_LIBERTY_1_1:
            case LASSO_PROTOCOL_LIBERTY_1_2:
                $meta = LassoSPKitMetadataLiberty::generateMetadata($base, $organization, $publickey);
                LassoSPKitConfig::set('conformance', 'liberty');
                break;
            default:
                throw new Exception("illegal argument");
        }
        $ok = file_put_contents($spmeta, $meta);
        if (! $ok) {
            $error = "GenerateSPMetadata: Cannot save generated sp metadatas in $spmeta ";
            return 0;
        }
        return 1;
    }
    function makeConfig($organization, $idpMeta, $idpMetaUrl, $removePrivateKey, &$error) {
        $base = lassospkit_datadir();
        // Get idp metadatas
        $file = $this->getIdpMeta($idpMeta, $idpMetaUrl, $conformance, $error);
        if (! $file) {
            return 0;
        }
        $metabase = LassoSPKitHelper::getMetadataDir($conformance);
        if (! is_dir($metabase)) {
            if (! @mkdir($metabase, 0755, 1)) {
                $error = "MakeConfig: Cannot create directory $metabase";
                return 0;
            }
        }
        if (! LassoSPKitUtils::checkCanWrite($metabase, $error)) {
            return 0;
        }
        if (! LassoSPKitUtils::checkCanWrite($base, $error)) {
            return 0;
        }
        $idpmeta = LassoSPKitHelper::getIdpMetadataFile();
        if (! ($contents = @file_get_contents($file)) || ! @file_put_contents($idpmeta, $contents)) {
            $error = "MakeConfig: Cannot move idp temporary files to final place";
            return 0;
        }
        @unlink($file);
        // Create local metadatas
        $pkey = $metabase . "/" . PRIVATE_KEY;
        if (! is_file($pkey) || $removePrivateKey) {
            @unlink($pkey);
            if (! LassoSPKitUtils::generatePrivateKey($pkey, $error)) {
                return 0;
            }
        }
        $publickey = null; // Will contain the publickey
        if (! LassoSPKitUtils::extractPublicKey($pkey, $publickey, $error)) {
            return 0;
        }
        if (! $this->generateSPMetadata($conformance, 
                    LassoSPKitConfig::get('organization'), 
                    $publickey, 
                    $error)) {
            return 0;
        }
        return 1;
    }
}
