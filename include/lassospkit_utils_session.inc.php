<?php
require_once("lassospkit_config.inc.php");
require_once("lassospkit_datadir.inc.php");
require_once("lassospkit_session_file.inc.php");
require_once("lassospkit_session_php.inc.php");
require_once("lassospkit_session_memcache.inc.php");
require_once("lassospkit_debug.inc.php");
/** This object encapsulate the communication between the frontend and the
 backend of the LassoSPkit.

 If you use autofederation, i.e if you let the backend store relations between
 NameID and your local userId, you would only use get/setUserID().

 If not you must store Federation,NameID and UserID together in the application
 code.

 */
class LassoSPKitUtilsSession {
    static $key = "__LassoSPKitSessionObject";
    static $THIS;
    var $vars;

    /** If we getted the last error, clear it. */
    var $clears = array();
    /** The supported keys */
    static $keys = array(
            'NameID'=>0,
            'UserID'=>0,
            'Federation'=>0,
            'LastError'=>0,
            'loginRelayState'=>0,
            'federateRelayState'=>0,
            'ssoRelayState'=>0,
            'sloRelayState'=>0,
            'defederationRelayState'=>0,
            'nidmanagementRelayState'=>0,
            'errorRelayState'=>0,
            'loginParams'=>0,
            'federateParams'=>0,
            'sloParams'=>0,
            'defederationParams'=>0,
            'LogoutMethod'=>0,
            'assertionAttributes'=>0);
    /** The keys that must not survive one communication (one set followed by one get). */
    static $keysToClearAfterGet = array(
            'LastError'=>0,
            'loginRelayState'=>0,
            'federateRelayState'=>0,
            'ssoRelayState'=>0,
            'sloRelayState'=>0,
            'defederationRelayState'=>0,
            'nidmanagementRelayState'=>0,
            'errorRelayState'=>0,
            'loginParams'=>0,
            'federateParams'=>0,
            'sloParams'=>0,
            'defederationParams'=>0);
    public static $use_session = 0;
    static $session_storage_class;
    static $storage;
    static $timeout = 3600;
    var $id = null;

    function __construct() {
        $content = null;
#        if (self::$use_session) {
#            $content = LassoSPKitSessionPHP::retrieve($this, self::$timeout);
#        } else {
#            $content = LassoSPKitSessionFile::retrieve($this, self::$timeout);
#        }
        $content = self::$storage->retrieve($this, self::$timeout);
        lassospkit_debuglog("Session construct $content", 1);
        if ($content) {
            $t = @unserialize($content);
            if ($t && is_array($t)) {
                $this->vars = $t;
            } else {
                // Malformed session object, reset
                $this->vars = array();
                $this->delete();
            }
        } else {
            $this->vars = array();
        }
    }
    /** In the finalizer, reset one-shot keys, then
        store the rest in the PHP session */
    function __destruct() {
        foreach ($this->clears as $k => $v) {
            unset($this->vars[$k]);
        }
        $content = serialize($this->vars);
        self::$storage->store($this, $content);
        lassospkit_debuglog("Session store $content", 1);
#        if (self::$use_session) {
#            LassoSPKitSessionPHP::store($this, $content);
#        } else {
#            LassoSPKitSessionFile::store($this, $content);
#        }
    }

    /** Get the singleton object to communicate
     with the backend/fronted of the LassoSPKit. */
    public static function getSingleton() {
        if (! self::$THIS) {
            self::$THIS = new LassoSPKitUtilsSession();
        }
        return self::$THIS;
    }
    function get($key) {
        if (! isset(self::$keys[$key])) {
            throw new Exception("The key $key is not supported by LassoSPKitUtilsSession");
        }
        if (isset(self::$keysToClearAfterGet[$key])) {
            $this->clearAtShutdown($key);
        }
        if (isset($this->vars[$key])) {
            return $this->vars[$key];
        }
        return null;
    }
    function set($key, $value) {
        if (! isset(self::$keys[$key])) {
            throw new Exception("The key $key is not supported by LassoSPKitUtilsSession");
        }
        if (isset(self::$keysToClearAfterGet[$key])) {
            $this->doNotClearAtShutdown($key);
        }
        $this->vars[$key] = $value;
    }
    function clearAtShutdown($key) {
        $this->clears[$key] = 1;
    }
    function doNotClearAtShutdown($key) {
        unset($this->clears[$key]);
    }
    /** Return the NameID resulting
     * of the last WebSSO or passed from the
     * application to other profiles thant SSO,
     * like SLO, FederationTermination or
     * NameIdManagement. */
    static function getNameID() {
        $athis = self::getSingleton();
        return $athis->get('NameID');
    }
    
    /* Helper static functions */

    function delete() {
#        if (self::$use_session) {
#            LassoSPKitSessionPHP::delete($this);
#        } else {
#            LassoSPKitSessionFile::delete($this);
#        }
        self::$storage->delete($this);

    }
    /** Clear the session object of all communication
     from the LassoSPKit. */
    function clear() {
        $this->delete();
        $this->id = null;
    }
    /** Reset the state of the session */
    function clean() {
        $this->clear();
        $this->vars = array();
    }
    static function logout() {
        $athis = self::getSingleton();
        $athis->clear();
    }
    static function setLogoutMethod($logoutMethod) {
        $athis = self::getSingleton();
        $athis->set('LogoutMethod',$logoutMethod);
    }
    static function getLogoutMethod() {
        $athis = self::getSingleton();
        return $athis->get('LogoutMethod');
    }
    /** Set the NameID to transmit. */
    static function setNameID($NameID) {
        $athis = self::getSingleton();
        $athis->set('NameID',$NameID);
    }
    static function getUserID() {
        $athis = self::getSingleton();
        return $athis->get('UserID');
    }
    static function setUserID($UserID) {
        $athis = self::getSingleton();
        $athis->set('UserID',$UserID);
    }
    static function getLastError() {
        $athis = self::getSingleton();
        return $athis->get('LastError');
    }
    static function setLastError($LastError) {
        $athis = self::getSingleton();
        $athis->set('LastError',$LastError);
    }
    static function getFederation() {
        $athis = self::getSingleton();
        return $athis->get('Federation');
    }
    static function setFederation($Federation) {
        $athis = self::getSingleton();
        $athis->set('Federation',$Federation);
    }
    static function getRelayState($profile) {
        $athis = self::getSingleton();
        return $athis->get($profile . "RelayState");
    }
    static function setRelayState($profile, $RelayState) {
        $athis = self::getSingleton();
        $athis->set($profile."RelayState", $RelayState);
    }
    static function getParams($profile) {
        $athis = self::getSingleton();
        return $athis->get($profile . "Params");
    }
    static function setParams($profile, $Params) {
        $athis = self::getSingleton();
        $athis->set($profile ."Params", $Params);
    }
    static function setAssertionAttributes($attributes) {
        $athis = self::getSingleton();
        $athis->set('assertionAttributes', $attributes);
    }
    static function getAssertionAttributes() {
        $athis = self::getSingleton();
        return $athis->get('assertionAttributes');
    }
}

LassoSPKitUtilsSession::$session_storage_class = LassoSPKitConfig::get('session_storage_class');
LassoSPKitUtilsSession::$storage = new LassoSPKitUtilsSession::$session_storage_class();
if (LassoSPKitUtilsSession::$use_session && ! isset($_SESSION)) {
    session_start();
} else {
    LassoSPKitUtilsSession::getSingleton();
}
