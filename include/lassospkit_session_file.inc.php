<?php
require_once('lassospkit_datadir.inc.php');
require_once('lassospkit_debug.inc.php');



class LassoSPKitSessionFile {
    function setcookie($session, $timeout) {
        $LassoSPKitSessionFile_cookiename = 'lassospkit_file_' . LassoSPKitConfig::get('cookiename');
        static $done = 0;
        if ($done == 0) {
            $done = 1;
            $LassoSPKitSessionFile_cookiename = 'lassospkit_file_' . LassoSPKitConfig::get('cookiename');
            setcookie($LassoSPKitSessionFile_cookiename, $session->id, time()+$timeout, '/');
        }
    }
    function retrieve($session, $timeout) {
        $LassoSPKitSessionFile_cookiename = 'lassospkit_file_' . LassoSPKitConfig::get('cookiename');
        $content = null;
        if (isset($_COOKIE[$LassoSPKitSessionFile_cookiename])) {
            $session->id = $_COOKIE[$LassoSPKitSessionFile_cookiename];
            $valid = ereg("^[[:alnum:]]+$",$session->id);
            if ($valid) {
                $filepath = lassospkit_datadir() . "/cookie_session_" . $session->id;
                if (file_exists($filepath) && time()-filemtime($filepath) < $timeout) {
                    $content = @file_get_contents($filepath);
                    if ($content === FALSE) {
                        lassospkit_errlog("cannot read $filepath");
                    }
                } else {
                    self::delete($session);
                }
            }
        }
        if (! $content) {
            $session->id = md5("lasso" . rand());
        }
        LassoSPKitSessionFile::setcookie($session, $timeout);
        return $content;
    }
    function store($session, $content) {
        if ($session->id) {
            $ret = @file_put_contents(lassospkit_datadir() . "/cookie_session_" . $session->id, $content);
            if ($ret === FALSE) {
                lassospkit_errlog("cannot write into " . lassospkit_datadir() . "/cookie_session_" . $session->id);
            }
        }
    }
    function delete($session) {
        $filepath = lassospkit_datadir() . "/cookie_session_" . $session->id;
        @unlink($filepath);
    }
}
