<?
require_once('lassospkit_storage.inc.php');
require_once('lassospkit_datadir.inc.php');

/** Hard links needs to be supported on the filesystem. */
class LassoSPKitFileStore implements LassoSPKitStore {
    public $base = '';
    function __construct() {
        $this->base = lassospkit_datadir();
    }
    private function filepath($key) {
        return $this->base . '/' . $this->filename($key);
    }
    private function filename($key) {
        return 'lib_session_' . $key;
    }
    function get($key) {
        $content = @file_get_contents($this->filepath($key));
        if ($content === FALSE) {
            return null;
        }
        return @unserialize($content);
    }
    function set($key, $value) {
        if ($key && $key != "") {
            $path = $this->filepath($key);
            @unlink($path);
            $ret = @file_put_contents($path, @serialize($value));
            $this->debug($ret, "cannot set contents of file " . $this->filepath($key));
        } else {
            throw new Exception("Bad usage of FileStore::set, key is null");
        }
    }
    function delete($key) {
        @unlink($this->filepath($key));
    }
    function alias($key,$alias) {
        $target = $this->filepath($key);
        $sym = $this->filepath($alias);
        @unlink($sym);
        $ret = @link($target,$sym);
        $this->debug($ret, "could not alias key $target => $sym");
        return $ret;
    }
    function rename($old,$new) {
        $old_path = $this->filename($old);
        $new_path = $this->filename($new);
        $ret = @rename($old_path, $new_path);
        $this->debug($ret, "could not rename key $old => $new");
        return $ret;
    }
    function debug($ret, $mesg) {
        if ($ret === FALSE) {
            lassospkit_debuglog("SPKit File Storage: " . $mesg, 1);
        }
    }
    function linkcount($key) {
        $stat = stat($this->filepath($key));
        if (is_array($stat)) {
            return $stat[3];
        }
        return 0;
    }
}
?>
