<?php
require_once('lassospkit_generic_session.inc.php');
require_once('lassospkit_utils_session.inc.php');
require_once('lassospkit_debug.inc.php');
require_once('lassospkit_file.inc.php');
require_once('lassospkit_config.inc.php');
require_once('lassospkit_generic_session.inc.php');

class LassoSPKitAutoPersistentSession extends LassoSPKitDummySession {
    var $storage;
    function __construct() {
        $storage_class = "LassoSPKit" . LassoSPKitConfig::get('storage') . "Store";
        $this->storage = @new $storage_class();
        if ($this->storage == null) {
            throw new Exception("Config error: storage class $storage_class does not exist");
        }
    }
    function findFederation($nameID) {
        $federation = null;
        if ($nameID) {
            lassospkit_debuglog("looking for session using nameID from profile: $nameID", 1);
            $federation = $this->storage->get($nameID);
        }
        if (! $federation) {
            $userID = LassoSPKitUtilsSession::getUserID();
            if ($userID) {
                lassospkit_debuglog("looking for session using userID: $userID", 1);
                $federation = $this->storage->get(md5($userID));
            }
        }
        if (! $federation) {
            $nameIDs = LassoSPKitUtilsSession::getNameID();
            if (is_array($nameIDs)) {
                foreach (LassoSPKitUtilsSession::getNameID() as $nameID) {
                    lassospkit_debuglog("looking for session using nameID in session: $userID", 1);
                    $federation = $this->storage->get($nameID);
                }
            }
        }
        if ($federation == null) {
            lassospkit_debuglog('did not found any session file...', 1);
            return 0;
        }
        $this->explodeFederation($federation);
        return 1;
    }
    function saveFederation() {
        $nameIDs = $this->getNameIDs();
        $firstID = array_pop($nameIDs);
        if ($firstID) {
            $federation = $this->getFederationArray();
            $this->storage->set($firstID, $federation);
            foreach ($nameIDs as $otherID) {
                $this->storage->alias($firstID, $otherID);
            }
            $userID = LassoSPKitUtilsSession::getUserID();
            if ($userID) {
                $this->storage->alias($firstID, md5($userID));
            }
        }
        parent::saveFederation();
    }
    function changeFederation($oldID, $newID) {
        if ($newID) {
            $this->storage->rename($oldID, $newID);
        } else {
            $blob = $this->storage->get($oldID);
            $this->storage->delete($oldID);
            $userid = $blob['userid'];
            if ($userid) {
                $md5 = md5($userid);
                if ($this->storage->linkcount($md5) == 1) {
                    $this->storage->delete($md5);
                    return;
                } 
            }
        }
        parent::saveFederation();
    }
    function logout() {
        LassoSPKitUtilsSession::logout();
    }
}
