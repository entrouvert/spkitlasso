<?
interface LassoSPKitStore {
    public function get($key);
    public function set($key, $value);
    public function delete($key);
    public function alias($key1,$key2);
    public function rename($key1, $key2);
    public function linkcount($key);
}

?>
