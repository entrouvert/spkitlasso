<?php

require_once('lassospkit_config.inc.php');
require_once('lassospkit_debug.inc.php');

class LassoSPKitMemCache {
    function getInstance() {
        static $instance = null;

        if ($instance == null) {
            $instance = new Memcache();
            $memcache_servers = LassoSPKitConfig::get('memcache_servers');
            if (! $memcache_servers) {
                lassospkit_errlog('There is no configuration for memcache servers, put one');
                throw new Exception('memcache config error');
            }
            $servers = self::validateServers($memcache_servers);
            foreach ($servers as $hostport) {
                $host = $hostport[0];
                $port = $hostport[1];
                $res = $instance->pconnect($host,intval($port));
                if ($res === FALSE) {
                    lassospkit_errlog("LassoSPKitMemcache: could not connect to $host:$port");
                }
            }
        }
        return $instance;
    }
    function validateServers($str) {
        $pairs = split(" +", $str);
        $servers = array();

        foreach ($pairs as $a_pair) {
            if (! ereg("^([[:alnum:]]+):([[:digit:]]+)$",$a_pair,$matches) ||
                intval($matches[2]) == 0) {
                lassospkit_errlog("$a_pair is not a valid memcache server ref");
            } else {
                $servers[] = array($matches[0],$matches[1]);
            }
        }
        if (count($servers)) {
            return $servers;
        } else {
            return null;
        }
    }
    function add($key, $value, $timeout = 2592000) {
        $instance = LassoSPKitMemCache::getInstance();
        if (! $instance) {
            lassospkit_errlog("LassoSPKitMemCache: could not add key " . var_export($key,1) . ", no instance present");
        }
        $res = $instance->add($key, $value, false, $timeout);
        return $res;
    }
    function set($key, $value, $timeout = 2592000) {
        $instance = LassoSPKitMemCache::getInstance();
        if (! $instance) {
            lassospkit_errlog("LassoSPKitMemCache: could not set key " . var_export($key,1) . ", no instance present");
        }
        $res = $instance->set($key, $value, false, $timeout);
        return $res;
    }
    function get($key) {
        $instance = LassoSPKitMemCache::getInstance();
        if (! $instance) {
            lassospkit_errlog("LassoSPKitMemCache: could not get key " . var_export($key,1) . ", no instance present");
        }
        return $instance->get($key);
    }
    function delete($key) {
        $instance = LassoSPKitMemCache::getInstance();
        if (! $instance) {
            lassospkit_errlog("LassoSPKitMemCache: could not delete key " . var_export($key,1) . ", no instance present");
        }
        return $instance->delete($key);
    }
}
