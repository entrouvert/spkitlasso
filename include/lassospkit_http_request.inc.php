<?php

class LassoSPKitHttpRequest {
    var $url;
    var $header = array();
    var $method = 'GET';
    var $mime_type = null;
    var $_content = null;


    function init($url = null) {
        if ($url) {
            $this->url = $url;
        } else {
            throw new Exception("LassoSPKitHttpRequest: __construct without arg not implemented");
        }
        $this->getCurrentCookies();
    }
    function setPOST($mime_type, $content) {
        $this->method = 'POST';
        $this->mime_type = $mime_type;
        $this->_content = $content;
    }
    function getCurrentCookies() {
        $this->headers = array();
        foreach ($_COOKIE as $cookie => $value) {
            $this->headers[] = "Cookie: " . urlencode($cookie) . "=" . urlencode($value);
        }
    }
    function perform() {
        $opts = array('http' =>
                    array(
                        'method'  => $this->method,
                        'header'  => $this->header,
                        'content' => $this->_content
                        )
                );
        $context  = stream_context_create($opts);
        $res = @file_get_contents($this->url,false,$context);
        return $res;
    }
    function buildPOST($url,$mime_type,$content) {
        $b = new LassoSPKitHttpRequest();
        $b->init($url);
        $b->setPost($mime_type,$content);
        return $b;
    }
    function buildGET($url) {
        $b = new LassoSPKitHttpRequest();
        $b->init($url);
        return $b;
    }
}
