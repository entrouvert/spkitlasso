<?php
require_once('lassospkit_datadir.inc.php');
require_once('lassospkit_debug.inc.php');

$LassoSPKitSessionPHP_key = "__LassoSPKitSessionObject";

class LassoSPKitSessionPHP {
    function retrieve($session, $timeout) {
        global $LassoSPKitSessionPHP_key;

        $content = null;
        #if (! isset($_SESSION)) {
        #    throw new Exception("LassoSPKit cannot work without PHP sessions if use_session is TRUE.");
        #}
        if (isset($_SESSION[$LassoSPKitSessionPHP_key])) {
            $content = $_SESSION[$LassoSPKitSessionPHP_key];
            if (! isset($_SESSION[$LassoSPKitSessionPHP_key . '_time']) ||
                $_SESSION[$LassoSPKitSessionPHP_key . '_time'] - time() > $timeout) {
                    $content = null;
                    self::delete($session);
                }
        }
        return $content;
    }
    function store($session, $content) {
        global $LassoSPKitSessionPHP_key;
        $_SESSION[$LassoSPKitSessionPHP_key] = $content;
        $_SESSION[$LassoSPKitSessionPHP_key . '_time'] = time();
    }
    function delete($session) {
        unset($_SESSION[$LassoSPKitSessionPHP_key]);
        unset($_SESSION[$LassoSPKitSessionPHP_key . '_time']);
    }
}
