<?php
require_once('lassospkit_datadir.inc.php');
require_once('lassospkit_debug.inc.php');
require_once('lassospkit_memcache.inc.php');



class LassoSPKitSessionMemCache {
    function getkey($session) {
        return $session->id . "_cookie_session";
    }
    function retrieve($session, $timeout) {
        $LassoSPKitSessionMemCache_cookiename = 'lassospkit_memcache_' . LassoSPKitConfig::get('cookiename');

        lassospkit_debuglog("Retrieving $LassoSPKitSessionMemCache_cookiename : " . var_export($_COOKIE, 1), 1);
        $content = null;
        if (isset($_COOKIE[$LassoSPKitSessionMemCache_cookiename])) {
            $session->id = $_COOKIE[$LassoSPKitSessionMemCache_cookiename];
            lassospkit_debuglog("Retrieving " . $session->id, 1);
            $valid = ereg("^[[:alnum:]]+$",$session->id);
            if ($valid) {
                $memcache_key = self::getkey($session);
                $content = LassoSPKitMemCache::get($memcache_key);
            }
            if (! $content) {
                self::delete($session);
            }
        }
        if (! $content) {
            $session->id = md5("lasso" . rand());
            setcookie($LassoSPKitSessionMemCache_cookiename, $session->id, time()+3600, '/');
        }
        return $content;
    }
    function store($session, $content) {
        if ($session->id) {
            $memcache_key = self::getkey($session);
            $ret = LassoSPKitMemCache::set($memcache_key, $content, LassoSPKitUtilsSession::$timeout);
            if ($ret === FALSE) {
                lassospkit_errlog("cannot write into Memcache for key cookie_session_" . $session->id);
            }
        }
    }
    function delete($session) {
        $memcache_key = self::getkey($session);
        LassoSPKitMemCache::delete($memcache_key);
    }
}
