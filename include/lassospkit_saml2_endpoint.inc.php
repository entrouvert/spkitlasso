<?php
require_once('lassospkit_debug.inc.php');
require_once('lassospkit_endpoints.inc.php');
require_once('lassospkit_metadata.inc.php');
require_once('lassospkit_saml2.inc.php');

class LassoSPKitSaml2Endpoint extends LassoSPKitEndpoint {
    function LassoSPKitSaml2Endpoint() {
        parent::LassoSPKitEndpoint();
        $this->addDispatch('/nameIdManagementBrws','nameIdManagementBrws');
        $this->addDispatch('/nameIdManagementSoap','nameIdManagementSoap');
        $this->addDispatch('/nameIdManagementReturn','nameIdManagementReturn');
        $this->addDispatch('/metadata','metadata');
    }
    function getProfileObject() {
        static $profile = null;
        if ($profile == null) {
            $profile =  new LassoSPKitSAML2($this->getSession());
        }
        return $profile;
    }
    /** Generate metadatas for the SAML2 endpoint */
    function metadata() {
        $datadir = LassoSPKitHelper::getMetadataDir(LASSO_PROTOCOL_SAML_2_0);
        $pkey = $datadir . "/" . PRIVATE_KEY;
        LassoSPKitUtils::extractPublicKey($pkey, $publickey, $error);
        try { 
            $content = LassoSPKitMetadataSAML2::generateMetadata(LassoSPKitConfig::get('baseUrl'), LassoSPKitConfig::get('organization'), array('publickey' => $publickey));
            if ($content) {
                header('Content-type: text/xml');
                echo $content;
            }
        } catch (Exception $e) {
            echo 'An exception occured';
            lassospkit_errlog($e);
        }
    }
    function nameIdManagementBrws() {
        // TODO
    }
    function nameIdManagementSoap() {
        $ret = 0;
        try { 
            $profile = $this->getProfileObject();
            $ret = $profile->processSOAPRequestNameIdManagement();
        } catch (LassoError $e) {
            lassospkit_debuglog('Critical error: ' . $e, 1);
            $ret = $e->getCode();
        } catch (Exception $e) {
            lassospkit_debuglog('Unkown exception error: ' . $e, 1);
            $ret = -1;
        }
        return $this->handleNameIdManagement($ret);
    }
    function nameIdManagementReturn() {
        // TODO
    }
    function handleNameIdManagement($ret) {
        return $ret;
    }
}
