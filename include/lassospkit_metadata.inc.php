<?php
require_once('lassospkit_config.inc.php');

class LassoSPKitMetadataSAML2 {
    static function generateMetadata($baseUrl, $orgname, $options = array()) {
        if ( ! (is_string($baseUrl) && is_string($orgname))) {
            throw new Exception("Bad parameters to generate metadatas");
        }
        $default_options = array(
            'publickey' => null,
            'ssoActivated' => true,
            'sloActivated' => true,
            'nidActivated' => true,
            'contacts' => array(),
            'nidFormats' => array(LASSO_SAML2_NAME_IDENTIFIER_FORMAT_PERSISTENT,LASSO_SAML2_NAME_IDENTIFIER_FORMAT_TRANSIENT));
        $final_options = array_merge($default_options, $options);
        extract($final_options);

        $prefix = '/saml2';
        if (LassoSPKitConfig::get('showExtension')) {
            $prefix .= '.php';
        }
        $meta = '<?xml version="1.0"?>
            <EntityDescriptor xmlns="urn:oasis:names:tc:SAML:2.0:metadata"
            xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion"
            xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
            entityID="' . $baseUrl . $prefix . '/metadata">
            <SPSSODescriptor
            AuthnRequestsSigned="true"
            protocolSupportEnumeration="urn:oasis:names:tc:SAML:2.0:protocol">';
        if ($publickey) {
            $meta .= '
                <KeyDescriptor use="signing">
                <ds:KeyInfo xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
                <ds:KeyValue>' . $publickey . '</ds:KeyValue>
                </ds:KeyInfo>
                </KeyDescriptor>
                ';
        }
        foreach ($nidFormats as $nidFormat) {
            $meta .= '
                <NameIdFormat>' . $nidFormat . '</NameIdFormat>
                ';
        }
        foreach ($contacts as $contact) {
            $meta .= '
                <ContactPerson><EmailAddress>' . $contact . '</EmailAddress></ContactPerson>
                ';
        }
        $url = $baseUrl . $prefix;
        if ($ssoActivated) {
            $meta .= self::assertionConsumerService("$url/assertionConsumer");
        }
        if ($sloActivated) {
            $meta .= self::singleLogout("$url/sloSoap", LASSO_SAML2_METADATA_BINDING_SOAP);
            $meta .= self::singleLogout("$url/sloBrws", LASSO_SAML2_METADATA_BINDING_REDIRECT, "$url/sloReturn");
            $meta .= self::singleLogout("$url/sloBrws", LASSO_SAML2_METADATA_BINDING_POST, "$url/sloReturn");
            $meta .= self::singleLogout("$url/sloBrws", LASSO_SAML2_METADATA_BINDING_ARTIFACT, "$url/sloReturn");
        }
        if ($nidActivated) {
            $meta .= self::nameIdManagement("$url/nameIdManagementSoap", LASSO_SAML2_METADATA_BINDING_SOAP);
            $meta .= self::nameIdManagement("$url/nameIdManagementBrws", LASSO_SAML2_METADATA_BINDING_REDIRECT, "$url/nameIdManagementReturn");
            $meta .= self::nameIdManagement("$url/nameIdManagementBrws", LASSO_SAML2_METADATA_BINDING_POST, "$url/nameIdManagementReturn");
            $meta .= self::nameIdManagement("$url/nameIdManagementBrws", LASSO_SAML2_METADATA_BINDING_ARTIFACT, "$url/nameIdManagementReturn");
        }

        $meta .= '
            </SPSSODescriptor>
            <Organization>
            <OrganizationName>' . $orgname . '</OrganizationName>
            </Organization>
            </EntityDescriptor>';
        return $meta;
    }
    function service($name, $place, $binding, $return = null) {
            $ret = "";
            $ret .= '<' . $name . ' Binding="';
            $ret .= $binding;
            $ret .= '" Location="' . $place . '" ';
            if ($return) {
                $ret .= 'ResponseLocation="' . $return . '"';
            }
            $ret .= '/>';
            return $ret;
    }
    function singleLogout($place, $binding, $return = null) {
            return self::service('SingleLogoutService', $place, $binding, $return);
    }
    function nameIdManagement($place, $binding, $return = null) {
            return self::service('ManageNameIDService', $place, $binding, $return);
    }
    function assertionConsumerService($place, 
                                      $binding = LASSO_SAML2_METADATA_BINDING_ARTIFACT, 
                                      $return = null) {
            return '<AssertionConsumerService isDefault="true" index="0"
            Binding="'. $binding .'"
            Location="' . $place . '" />';
    }
}

class LassoSPKitMetadataLiberty {
    static function generateMetadata($baseUrl, $orgname, $publickey) {
        $prefix = '/liberty';
        if (LassoSPKitConfig::get('showExtension')) {
            $prefix .= '.php';
        }
        $meta = "";
        $meta .= 
'<?xml version="1.0"?>
<EntityDescriptor
providerID="' . $baseUrl . $prefix . '/metadata" xmlns="urn:liberty:metadata:2003-08" xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
<SPDescriptor>';
        // Public key
        $meta .= 
'<KeyDescriptor use="signing">
    <ds:KeyInfo>
        <ds:KeyValue>' . htmlspecialchars($publickey) . '</ds:KeyValue>
        </ds:KeyInfo>
        </KeyDescriptor>';
        // Endpoints
        $meta .= 
'<SOAPEndpoint>' . $baseUrl .$prefix . '/soap</SOAPEndpoint>
<SingleLogoutServiceURL>' . $baseUrl . $prefix . '/sloRedirect</SingleLogoutServiceURL>
<SingleLogoutServiceReturnURL>'. $baseUrl .$prefix . '/sloResponse</SingleLogoutServiceReturnURL>
<FederationTerminationServiceURL>'. $baseUrl.$prefix . '/defederateNotification</FederationTerminationServiceURL>
<FederationTerminationServiceReturnURL>'. $baseUrl.$prefix . '/defederateReturn</FederationTerminationServiceReturnURL>
<AssertionConsumerServiceURL>' . $baseUrl . $prefix . '/ssoAssertionConsumer</AssertionConsumerServiceURL>
';
        // TODO select supported profiles by config
        $meta .= "<SingleLogoutProtocolProfile>http://projectliberty.org/profiles/slo-sp-http</SingleLogoutProtocolProfile>\n";
        $meta .= "<SingleLogoutProtocolProfile>http://projectliberty.org/profiles/slo-sp-soap</SingleLogoutProtocolProfile>\n";
        $meta .= "<SingleLogoutProtocolProfile>http://projectliberty.org/profiles/slo-idp-http</SingleLogoutProtocolProfile>\n";
        $meta .= "<SingleLogoutProtocolProfile>http://projectliberty.org/profiles/slo-idp-soap</SingleLogoutProtocolProfile>\n";
        $meta .= "<FederationTerminationNotificationProtocolProfile>http://projectliberty.org/profiles/fedterm-sp-http</FederationTerminationNotificationProtocolProfile>\n";
        $meta .= "<FederationTerminationNotificationProtocolProfile>http://projectliberty.org/profiles/fedterm-sp-soap</FederationTerminationNotificationProtocolProfile>\n";
        $meta .= "<FederationTerminationNotificationProtocolProfile>http://projectliberty.org/profiles/fedterm-idp-http</FederationTerminationNotificationProtocolProfile>\n";
        $meta .= "<FederationTerminationNotificationProtocolProfile>http://projectliberty.org/profiles/fedterm-idp-soap</FederationTerminationNotificationProtocolProfile>\n";
        $meta .= "<AuthnRequestsSigned>true</AuthnRequestsSigned>
</SPDescriptor>
</EntityDescriptor>";

        return $meta;
    }
}
