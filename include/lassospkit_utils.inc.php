<?php
require_once('lassospkit_defines.inc.php');

class LassoSPKitUtils {
    function myself() {
        return 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
    }
    function mydir() {
        return 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
    }
    function relativePathToURL($rel) {
        $abs = "";
        if (isset($_SERVER['HTTPS'])) {
            $abs = "https://";
        } else {
            $abs = "http://";
        }
        $abs = $abs . $_SERVER['HTTP_HOST'];
        $abs = $abs . dirname($_SERVER['PHP_SELF']) . "/";
        $abs = $abs . $rel;
        return $abs;
    }
    function checkCanWrite($dir, &$error) {
        $path = $dir . "/"  . _CHECK_FILENAME;
        $ok = ! file_exists($path) || unlink($path);
        $ok = $ok && $file = fopen($path,"w");
        $ok = $ok && fclose($file);
        unlink($path);
        if (! $ok) {
            $error = "Cannot write into " . $dir;
        }
        return $ok;
    }
    function generatePrivateKey($file,&$error) {
        if (! is_file(OPENSSL_BIN)) {
            $error = "SSL Generate: Can't find OpenSSL at " . OPENSSL_BIN;
            return 0;    
        }
        exec('/usr/bin/openssl genrsa -out ' . $file . ' 2048 2>/dev/null', $foo, $ret);
        if ($ret != 0) {
            $error = "SSL Generate: OpenSSL returned non-0 while computing the private key, check your ssl installation.";
            return 0;
        }
        return 1;
    }
    function extractPublicKey($file,&$public,&$error) {
        if (! is_file(OPENSSL_BIN)) {
            $error = "SSL Generate: Can't find OpenSSL at " . OPENSSL_BIN;
            return 0;    
        }
        $tempfname = tempnam(TEMPDIR, "lassospkit-public-key");
        $cmdline = '/usr/bin/openssl rsa -in ' . $file . ' -pubout -out ' . $tempfname . ' 2>/dev/null';
        exec($cmdline, $foo, $ret);
        if ($ret != 0) {
            $error = "SSL Generate: OpenSSL return non-0 while extracting the public key from the private key file, check your ssl installation. $cmdline";
            return 0;
        }
        if (! $public = file_get_contents($tempfname)) {
            $error = "SSL Generate: Extracted public key is empty. See $tempfname.";
            return 0;
        }
        @unlink($tempfname);
        return 1;
    }
    function extract_options($template, $source) {
        $ret = array();
        if (! is_array($source)) {
            return $ret;
        }
        foreach ($template as $key => $type) {
            if (isset($source[$key])) {
                $value = $source[$key];
                $ok = FALSE;
                switch ($type) {
                    case 'b':
                        $ok = is_bool($value);
                        break;
                    case 's':
                        $ok = is_string($value);
                        break;
                    case 'i':
                        $ok = is_int($value);
                        break;
                }
                if ($ok) {
                    $ret[$key] = $value;
                }
            }
        }
        return $ret;
    }
}
