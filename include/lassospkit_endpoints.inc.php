<?php
require_once('lassospkit_helper.inc.php');
require_once('lassospkit_debug.inc.php');
require_once('lassospkit_utils.inc.php');
require_once('lassospkit_url_dispatch.inc.php');
require_once('lassospkit_generic_session.inc.php');
require_once('lassospkit_dummysession.inc.php');
require_once('lassospkit_autopersistentsession.inc.php');

class LassoSPKitEndpoint extends LassoSPKitUrlDispatch {
    var $relayState = null;
    var $currentHttpMethod;

    function LassoSPKitEndpoint() {
        $this->addDispatch('/assertionConsumer', 'assertionConsumer');
        $this->addDispatch('/sloBrws','sloBrws');
        $this->addDispatch('/sloSoap','sloSoap');
        $this->addDispatch('/sloReturn','sloReturn');
    }

    /** Return the session object. */
    function getSession() {
        static $session;
        if ($session == null) {
            $session = $this->buildSession();
        }
        return $session;
    }
    function buildSession() {
        $session_class = "LassoSPKit" . LassoSPKitConfig::get('session');
        return new $session_class();
    }
    function distpatchAndExit() {
        if (isset($_SERVER['HTTP_REFERER'])) {
            $host = $_SERVER['HTTP_REFERER'];
            $this->verifyUrl($host);
        }
        parent::dispatchAndExit();
    }
    /** Verify that the host is the same has HTTP_HOST */
    function verifyUrl($host) {
        $host = strstr('//', $host);
        $pos = strpos($host, '/');
        if ($pos !== FALSE) {
            $host = substr($host, 0, $pos);
        }

        if ($host && isset($_SERVER['HTTP_HOST']) && $host != $_SERVER['HTTP_HOST']) {
            echo "Bad referer '$host' != '" . $_SERVER['HTTP_HOST'] . "'";
            exit(1);
        }
    }
    /** Get the profile object, LassoSPKitSaml2 or LassoSPKitLiberty */
    function getProfileObject() {
        throw new Exception('Not implemented');
    }
    /** Implementation of an assertion consumer endpoint, it supports
     ARTIFACT, POST and GET methods */
    function assertionConsumer() {
        $ret = 0;
        $profile = null;
        try {
            $profile = $this->getProfileObject();
            $http_method = $this->limitMethodBrws($this->identifyHttpMethod());
            $query_string = $this->getQueryString($http_method);
            $ok = $profile->ssoConsumer($http_method, $query_string);
            $ret = $ok;
        } catch (LassoError $e) {
            $ret = $e->getCode();
        } catch (Exception $e) {
            $ret = -1;
        }
        $this->relayState = $profile->relayState;
        return $this->handleSso($ret);
    }
    /** Implementation of a SLO endpoint. It supports 
      ARTIFACT, GET and POST bindings. */
    function sloBrws() {
        $ret = 0;
        $profile = null;
        try {
            $profile = $this->getProfileObject();
            $http_method = $this->limitMethodBrws($this->identifyHttpMethod());
            $query_string = $this->getQueryString($http_method);
            $ret = $profile->processRequestSLO($http_method, $query_string);
            $this->relayState = $profile->relayState;
        } catch (LassoError $e) {
            $ret = $e->getCode();
        } catch (Exception $e) {
            $ret = -1;
        }
        $this->relayState = $profile->relayState;
        return $this->handleSlo($ret);
    }
    /** Implementation of a SLO endpoint. It supports 
      the SOAP binding. */
    function sloSoap() {
        $ret = 0;
        $profile = null;
        try { 
            $profile = $this->getProfileObject();
            $ret = $profile->processSOAPRequestSLO();
        } catch (LassoError $e) {
            lassospkit_debuglog('Critical error: ' . $e, 1);
            $ret = $e->getCode();
        } catch (Exception $e) {
            $ret = -1;
        }
        $this->relayState = $profile->relayState;
        return $this->handleSlo($ret);
    }
    /** Implementation of the SLO endpoint return when SLO is initiated
      by the SP, it supports the POST and GET binding. */
    function sloReturn() {
        $ret = 0;
        $profile = null;
        try {
            $profile = $this->getProfileObject();
            $http_method = $this->limitMethodBrws($this->identifyHttpMethod());
            $query_string = $this->getQueryString($http_method);
            $ret = $profile->processResponseSLO($http_method, $query_string);
        } catch (LassoError $e) {
            $ret = $e->getCode();
        } catch (Exception $e) {
            $ret = -1;
        }
        $this->relayState = $profile->relayState;
        return $this->handlSloReturn($ret);
    }
    /** Helper function to identify the HTTP method used to access the current
     * endpoint */
    function identifyHttpMethod() {
        $this->currentHttpMethod = -1;
        if (isset($_POST) && ( isset($_POST['SAMLResponse']) || isset($_POST['SAMLRequest']))) {
            $this->currentHttpMethod = LASSO_HTTP_METHOD_POST;
        }
        if (isset($_GET) && ( isset($_GET['SAMLResponse']) || isset($_GET['SAMLRequest']) )) {
            $this->currentHttpMethod = LASSO_HTTP_METHOD_REDIRECT;
        }
        if (isset($_GET) && ( isset($_GET['SAMLart']))) {
            $this->currentHttpMethod = LASSO_HTTP_METHOD_ARTIFACT_GET;
        }
        if (isset($_POST) && ( isset($_POST['SAMLart']))) {
            $this->currentHttpMethod = LASSO_HTTP_METHOD_ARTIFACT_POST;
        }
        return $this->currentHttpMethod;
    }
    /** Get the query string depending on the used HTTP method */
    function getQueryString($http_method) {
        switch ($http_method) {
            case LASSO_HTTP_METHOD_POST:
            case LASSO_HTTP_METHOD_ARTIFACT_POST:
                return @file_get_contents('php://input');
            case LASSO_HTTP_METHOD_REDIRECT:
            case LASSO_HTTP_METHOD_ARTIFACT_GET:
                return $_SERVER['QUERY_STRING'];
        }
        return null;
    }
    /** Restrict possible method for HTTP endpoints, 
     so forbid using SOAP on HTTP endpoints. */
    function limitMethodBrws($http_method) {
        switch ($http_method) {
            case LASSO_HTTP_METHOD_POST:
            case LASSO_HTTP_METHOD_ARTIFACT_POST:
            case LASSO_HTTP_METHOD_REDIRECT:
            case LASSO_HTTP_METHOD_ARTIFACT_GET:
                return $http_method;
        }
        return LASSO_HTTP_METHOD_NONE;
    }
    /** Dummy function to overload to handle the Sso */
    function handleSso($ret) {
        return $ret;
    }
    /** Dummy function to overload to handle the IdP
     *  initiated SLO. */
    function handleSlo($ret) {
        return $ret;
    }
    /** Dummy function to overload to handle the return
     from the IdP for SP initiated logout. */
    function handleSloReturn($ret) {
        return $ret;
    }
}
