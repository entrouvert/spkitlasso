<?
require_once('lassospkit_datadir.inc.php');
require_once('lassospkit_debug.inc.php');

/** This class represents the non-SAML-metadata part
  * of the config of the spkit. */

LassoSPKitConfig::$default_values['cookiename'] = md5("".rand());

class LassoSPKitConfig {
    static $default_values = array(
        'federate' => 'file', /* Does the backend persist federation ? no, file or mysql. */
        'mysql_host'=> 'localhost', /* Configuration of the MySql connection if federate = mysql */
        'mysql_user' => '',
        'mysql_password' => '',
        'mysql_database' => '',
        'mysql_table' => '_lassospkit_userid2nameid',
        'debug' => '0', /* Activate extra debugging */
        'organization' =>  "", /* Nom de l'organisation */
        'conformance' => "",
        'idp_metadata_url' => "",
        'baseUrl' => "",
        'session' => "AutoPersistentSession",
        'storage' => "File",
        'cookiename' => 0,
        'default_return_url' => null,
        'lasso_lib' => 'lasso.php',
        'showExtension' => 1, /* Shall we show the extension of scripts in public apis */
        'memcache_servers' => 'localhost:11211', /* Blank separated list of host:port pairs */
        'session_storage_class' => 'LassoSPKitSessionFile'
    );
    private static $instance = null;
    private static $file;

    function __construct() {
    }
    /** Explode the array $table into an inifile, do not
        try to encode values, they must be strings or null.
      */
    static function writeIni($path, $table) {
        $content = "";
        foreach ($table as $k => $v) {
            if ($v == null) {
                $content .= "$k=null\n";
            } else {
                $content .= "$k=$v\n";
            }
        }
        $ret = @file_put_contents($path, $content);
        if ($ret === FALSE) {
            lassospkit_errlog("Config: loadIni cannot write configuration file $path");
            throw new Exception("Cannot write $path");
        }
    }
    /** Load and parse file at $path. The file must an .ini file,
        i.e key value pairs separated by a '=' character, and pairs separated
        by a '\n' character. */
    static function loadIni($path) {
        $table = array();
        $content = @file_get_contents($path);
        if ($content === FALSE) {
            lassospkit_errlog("Config: loadIni cannot read configuration file $path");
            throw new Exception("Cannot read $path");
        }
        $lines = split("\n", $content);
        foreach ($lines as $line) {
            $pair = split("=", $line, 2);
            if (count($pair) == 2) {
                if ($pair[1] == 'null') {
                    $pair[1] = null;
                }
                $table[$pair[0]] = $pair[1];
            }
        }
        return $table;
    }
    /** If not existent load the .ini config file and fill the singleton table. */
    static function init() {
        if (! self::$instance) {
	    /* Where is lasso PHP binding ? */
            self::$file = lassospkit_datadir() . '/lassospkit_config.ini';
            if (file_exists(self::$file)) {
                self::$instance = self::loadIni(self::$file);
            } else {
                self::$instance = array();
            }
        }
    }
    /** Commit the content of the singleton table to the .ini file */
    static function commit() {
        self::writeIni(self::$file, self::$instance);
    }
    /** Remove a key fromt the config file. Future 'get' will
        return the default value. */
    function setDefault($name) {
        self::init();
        unset(self::$instance[$name]);
    }
    static function get($name) {
        self::init();
        if (! array_key_exists($name, self::$default_values)) {
            throw new Exception('Try to read an unknown config field');
        }
        if (array_key_exists($name, self::$instance)) {
            return self::$instance[$name];
        }
        return self::$default_values[$name];
    }
    static function set($name, $value) {
        self::init();
        if (! array_key_exists($name, self::$default_values)) {
            throw new Exception('Try to write an unknown config field');
        }
        self::$instance[$name] = $value;
    }
    public function keys() {
        return array_keys(self::$default_values);
    }
}

?>
