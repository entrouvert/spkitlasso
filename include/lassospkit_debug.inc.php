<?php
require_once('lassospkit_config.inc.php');
libxml_use_internal_errors(true);
error_reporting(E_ALL);

function lassospkit_debuglog($msg, $level = 0) {
    if (! is_string($msg)) {
        $msg = var_export($msg, 1);
    }
    $lassospkit_debug_level = LassoSPKitConfig::get('debug');
    if ($level <= $lassospkit_debug_level) {
        openlog("LassoPHP.SP.Kit", LOG_PID, LOG_AUTHPRIV);
        $msgs = explode("\n",$msg);
        foreach ($msgs as $msg) {
            syslog(LOG_DEBUG, $msg);
        }
        closelog();
    }
}
function lassospkit_errlog($msg) {
        openlog("LassoPHP.SP.Kit", LOG_PID, LOG_AUTHPRIV);
        syslog(LOG_ERR, $msg);
        closelog();
}
function lassospkit_infolog($msg) {
        openlog("LassoPHP.SP.Kit", LOG_PID, LOG_AUTHPRIV);
        syslog(LOG_INFO, $msg);
        closelog();
}

function lassospkit_showCode($code) {
    echo '<pre class="code">';
    echo htmlspecialchars($code);
    echo '</pre>';
}

//set_error_handler("my_error_handler", E_ALL);
function my_error_handler($errno, $errstr, $errfile, $errline){
    $errno = $errno & error_reporting();
    if($errno == 0) return;
    if(!defined('E_STRICT'))            define('E_STRICT', 2048);
    if(!defined('E_RECOVERABLE_ERROR')) define('E_RECOVERABLE_ERROR', 4096);
    switch($errno){
        case E_ERROR:               lassospkit_debuglog( "Error");                  break;
        case E_WARNING:             lassospkit_debuglog( "Warning");                break;
        case E_PARSE:               lassospkit_debuglog( "Parse Error");            break;
        case E_NOTICE:              lassospkit_debuglog( "Notice");                 break;
        case E_CORE_ERROR:          lassospkit_debuglog( "Core Error");             break;
        case E_CORE_WARNING:        lassospkit_debuglog( "Core Warning");           break;
        case E_COMPILE_ERROR:       lassospkit_debuglog( "Compile Error");          break;
        case E_COMPILE_WARNING:     lassospkit_debuglog( "Compile Warning");        break;
        case E_USER_ERROR:          lassospkit_debuglog( "User Error");             break;
        case E_USER_WARNING:        lassospkit_debuglog( "User Warning");           break;
        case E_USER_NOTICE:         lassospkit_debuglog( "User Notice");            break;
        case E_STRICT:              lassospkit_debuglog( "Strict Notice");          break;
        case E_RECOVERABLE_ERROR:   lassospkit_debuglog( "Recoverable Error");      break;
        default:                    lassospkit_debuglog( "Unknown error ($errno)"); break;
    }
    lassospkit_debuglog( ": $errstr in $errfile on line $errline\n");
    if(function_exists('debug_backtrace')){
        $backtrace = debug_backtrace();
        array_shift($backtrace);
        foreach($backtrace as $i=>$l){
            lassospkit_debuglog( "[$i] in function {$l['class']}{$l['type']}{$l['function']}");
            if($l['file']) lassospkit_debuglog( " in {$l['file']}");
            if($l['line']) lassospkit_debuglog( " on line {$l['line']}");
            lassospkit_debuglog( "\n");
        }
    }
    if(isset($GLOBALS['error_fatal'])){
        if($GLOBALS['error_fatal'] & $errno) die('fatal');
    }
}

