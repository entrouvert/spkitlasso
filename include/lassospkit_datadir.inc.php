<?php
    /** Function for getting the place of the config file, the federation files when autofederation is activated. 
     * Change this function to change way of find kit position. When using lassospkit
     * in an unique directory with you application, you can just make it return "../data".
     */
    function lassospkit_datadir() {
        $path = '/var/lib/spkitlasso/' . $_SERVER['HTTP_HOST'];
        // $path = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "data";
        if (! file_exists($path)) {
            @mkdir($path,0755,1);
        }
        return $path;
    }
