<?
require_once('lassospkit_config.inc.php');
require_once('lassospkit_debug.inc.php');
require_once('lassospkit_saml_common.inc.php');
require_once('lassospkit_lib.inc.php');

class LassoSPKitLiberty extends LassoSPKitSAMLCommon {
    public function __construct(LassoSPKitGenericSession  $session) {
        parent::__construct($session);
        $this->initServer(lassospkit_datadir() . "/liberty");
    }
    /** Overloaded method to serve as callback to common SSO method. */
    protected ssoNameIdPolicyConfig(LassoLogin $login, $blob) {
            $request = $login->request;
            $request->NameIDPolicy = $blob;
    }
    public function initiateFTNotification($method = LASSO_HTTP_METHOD_SOAP,  $remoteID = null) {
        $code = null;
        $response = null;
        $defederation = new LassoDefederation($this->server);
        $retFF = $this->findFederation($defederation);
        if ($retFF) {
            return $retFF;
        }
        $retIN = $defederation->initNotification($remoteID, $method);
        $retBN = $defederation->buildNotificationMsg();
        if ($retIN || $retBN) {
            lassospkit_errlog("initiateFTNotification, retIN: $retIN retBN: $retBN");
        }
        switch ($method) {
            case LASSO_HTTP_METHOD_REDIRECT:
                $this->keepProfile($defederation);
                $this->finishRedirectRequest($defederation);
                break;
            case LASSO_HTTP_METHOD_SOAP:
                $this->finishSOAPRequest($defederation, $response, $code);
                break;
            case LASSO_HTTP_METHOD_ARTIFACT_GET:
            case LASSO_HTTP_METHOD_ARTIFACT_POST:
            case LASSO_HTTP_METHOD_POST:
            default:
                LassoSPKitHelper::notImplemented();
        }
        LassoSPKitSamlCommon::initiateFTNotification($method, $remoteID);
        if ($retIN) {
            return $retIN;
        }
        if ($retBN) {
            return $retBN;
        }
        if ($code != "200") {
            lassospkit_errlog('initiateFTNotification, IdP returned non-200 HTTP result code: ' . $code);
            return -1;
        }
        return 0;
    }
}
