<?php
require_once('lassospkit_helper.inc.php');
require_once('lassospkit_utils.inc.php');
require_once('lassospkit_metadata.inc.php');

class LassoSPKitUrlDispatch {
    var $dispatch_table = array();
    
    function addDispatch($point, $method) {
        $this->dispatch_table[$point] = $method;
    }
    function dispatchAndExit() {
        if (! isset($_SERVER['PATH_INFO'])) {
            throw new Exception('No PATH INFO');
        }
        $path_info = $_SERVER['PATH_INFO'];
        $fname = $this->dispatch_table[$path_info];
        if ($fname) {
            $this->$fname();
            exit(0);
        } else {
            header("HTTP/1.0 404 Not Found");
            exit(0);
        }
    }
}
