<?php
require_once('../spkitlasso/lassospkit_saml2_endpoint.inc.php');
require_once('../spkitlasso/lassospkit_public_api.inc.php');

class EndpointExemple extends LassoSPKitSaml2Endpoint {
    function dispatchAndExit() {
        try {
            parent::dispatchAndExit();
        } catch (Exception $e) {
            $exceptionText = $e->__toString();
            $lines = explode("\n",$exceptionText);
            foreach ($lines as $line) {
                error_log($line);
            }
            die('There was an error, shoot yourself !');
        }
    }
    function headerHtml($title, $url) {
        ?>
        <html>
        <head>
        <title><?php echo $title ?></title>
        <meta http-equiv="refresh" content="5; url=<?php echo $url?>" />
        </head>
        <body> <?php
    }
    function footerHtml() {
        ?>
        </body>
        </html>
        <?php
    }
    function handleSso($ret) {
        $r = $this->relayState;
        $this->headerHtml("AssertionConsumer endpoint", $r);
        if ($ret == 0) {
            echo "Ouais on est loggé, aller <a href='$r'>$r</a>";
        } else {
            echo "Une erreur lasso s'est produite de code $ret" . strError($ret);
        }
        return $ret;
    }
    function handleSlo($ret) {
        $r = $this->relayState;
        if ($this->currentHttpMethod == LASSO_HTTP_METHOD_GET &&
            $this->currentHttpMethod == LASSO_HTTP_METHOD_POST) {
            $this->headerHtml("SLO endpoint", $r);
            if ($ret) {
                echo "Demande de slo échoué: " . strError($ret) . "($ret)";
            } else {
                echo "Demande de slo réussie";
            }
            echo "Go to <a href='$r'>$r</a>";
            $this->footerHtml();
            lassospkit_clean();
        } else {
            # Specialized
            $id = lassospkit_userid();
            error_log("Trying to destroy session $id");
            $session = LassoSPKitUtilsSession::getSingleton();
            $session->id = $id;
            $session->clean();
        }
        if ($ret) {
            error_log("Demande de slo échoué: $ret");
        } else {
            error_log("Demande de slo réussie: $ret");
        }

        return $ret;
    }
    function handleNameIdManagement($ret) {
        if ($ret) {
            error_log('Erreur lors du nid: ' . $ret . ': ' . strError($ret));
        }
        return $ret;
    }
}


$endpoint = new EndpointExemple();
$endpoint->dispatchAndExit();
