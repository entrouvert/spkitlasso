<?php
require_once('spkitlasso/lassospkit_public_api.inc.php');
require_once('spkitlasso/lassospkit_debug.inc.php');
require_once('spkitlasso/lassospkit_utils.inc.php');

if (isset($_SERVER['PATH_INFO']) && $_SERVER['PATH_INFO'] == '/logout') {
    lassospkit_soap_logout();
    header('Location: ' . $_SERVER['SCRIPT_NAME']);
    exit(0);
}
if (isset($_SERVER['PATH_INFO']) && $_SERVER['PATH_INFO'] == '/login') {
    $isPassive = null;
    if (isset($_GET['passive']) && $_GET['passive'] == 1) {
        $isPassive = true;
    }
    # Very special
    lassospkit_set_userid(LassoSPKitUtilsSession::getSingleton()->id);
    lassospkit_redirect_login('' . $_SERVER['SCRIPT_NAME'],$isPassive);
    exit(0);
}
if (isset($_SERVER['PATH_INFO']) && $_SERVER['PATH_INFO'] == '/federate') {
    $isPassive = null;
    if (isset($_GET['passive']) && $_GET['passive'] == 1) {
        $isPassive = true;
    }
    lassospkit_set_userid(LassoSPKitUtilsSession::getSingleton()->id);
    lassospkit_redirect_federate('' . $_SERVER['SCRIPT_NAME'],$isPassive);
    exit(0);
}
if (isset($_SERVER['PATH_INFO']) && $_SERVER['PATH_INFO'] == '/defederate') {
    lassospkit_soap_defederate();
    header('Location: ' . $_SERVER['SCRIPT_NAME']);
    exit(0);
}
echo '<?xml version="1.0" encoding="UTF-8"?>';

function show($a) {
    echo "<li><a href='$a'>";
    echo $a;
    echo "</a></li>";
}
?>

<html>
<head>
<link type="text/css" rel="stylesheet" href="style.css">
</head>
<h1>Points d'accès</h1>
<ul>
<?
echo lassospkit_datadir() . "<br/>";
show("exemple1.php/logout");
show("exemple1.php/login");
show("exemple1.php/federate");
show("exemple1.php/defederate");
show("exemple1.php/login?passive=1");

if (isset($_GET['userid'])) {
    lassospkit_set_userid($_GET['userid']);
}

$nameid = lassospkit_nameid();
$userid = lassospkit_userid();
$error = lassospkit_error();
$federation = lassospkit_federation();
if ($federation) {
    lassospkit_set_federation($federation);
}
?>
<li>NameId: <? echo lassospkit_showCode(var_export($nameid,1)) ?></li>
<li>UserId: <? echo lassospkit_showCode($userid) ?></li>
<li>Error: <? echo lassospkit_showCode($error) ?></li>
<li>Federation: <? echo lassospkit_showCode($federation) ?></li>
<li>Attributes: <? echo lassospkit_showCode(var_export(lassospkit_get_assertion_attributes(),1)); ?></li>
<li>Cookies: <? echo lassospkit_showCode(var_export($_COOKIE, 1)); ?></li>
</ul>

</html>
