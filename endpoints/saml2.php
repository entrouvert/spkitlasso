<?php
require_once('spkitlasso/lassospkit_saml2_endpoint.inc.php');
require_once('spkitlasso/lassospkit_public_api.inc.php');

$endpoint = new LassoSPKitSaml2Endpoint();
$endpoint->dispatchAndExit();
